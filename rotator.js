var articles = {};
var order = 0;
var data = [0];
var temp = {};

$.ajaxSetup({
  // Disable caching of AJAX responses
  cache: false
});

$('#top > header > nav').html('<svg id="bar" style="position: absolute; width: 100%; height: 39px; background-color: white; z-index: 30;"></svg>');
$("html").css("overflow", "hidden"); //odstranit scrollbar

function runBar() {
  d3.select('#bar').selectAll('*').remove();

  var bar = d3.select('#bar')
    .selectAll("rect")
    .data(data)
    .enter().append('rect')
    .attr('width', 0)
    .attr('height', 10)
    .attr('fill', '#d52834')
    .transition()
    .ease("linear")
    .duration(10000)
    .attr('width', $(window).width())
};

function getCont(key) {
	$.get(temp[key].link, function(data) {
		if ($(".b-data", data).length > 0) {
    		delete temp[key]; //vyhází longready
		} else {
			var tmp = $("article[role='article']:first", data).html();
	  		temp[key]['content'] = tmp;
		}
	}, 'html');
};

function loadRSS() {
  //test connection
  if (navigator.onLine == false) {
    console.log('no connection')
    return
  };
 	//load rss
  $.get("https://www.irozhlas.cz/sites/default/files/irozhlas_feeds/rss/main.xml", function(xml) {
    var lks = $(xml).find('link').slice(1, 8);
    for (var i = 0; i < 7; i++) {
      temp[i] = {'link': $(lks[i]).text(), 'content': null};
    };
    //load article details
    for (var key in temp) {
    	getCont(key);
    };
    articles = temp;
  }).fail(function() {
    console.log('internetu nevidno')
  });
};

function rotate() {
  //0 - 6
  var ks = Object.keys(articles);
  if (order > (ks.length - 1)) {
    order = 0;
  };
  $("article[role='article']:first").html(articles[ks[order]].content);
  //$('#qrcode').empty();
  //$('#qrcode').qrcode(articles[ks[order]].link);
  App.run({});
  runBar();
  order += 1;
};

$(document).ready(function() {
  $('.b-cookie').remove();
  loadRSS();
  /**
  $('body').append('<div id="qrcode"></div>')
  $('#qrcode').css({
    'position': 'absolute',
    'width:':'230px',
    'height': '230px',
    'left': '50px',
    'top': '127px',
  });
  **/
  setInterval(rotate, 10000);
  setInterval(loadRSS, 600000);
  setTimeout(function() {
    window.location.reload(true); //30 min reload kvůli bannerům, brandingům a dalším aktuálním radostem
  }, 1800000);
});